# N-Body Simulation #

An N-Body simulation is one simulating the gravitational interactions between N bodies, given each body's mass, position and initial speed.

This type of simulation is fundamental in Astrophysics and numerous research has tried to solve 
this problem in order to simulate the behavior of galaxies composed of millions of bodies. 

For instance, Max Planck Society's Supercomputing Centre managed to simulate the interactions between 10 billion galaxies 
using the computation power of a super calculator for a month (http://wwwmpa.mpa-garching.mpg.de/galform/virgo/millennium/index.shtml).

All this research has lead to the development of a lot of sophisticated algorithms. 

In this project, I have implemented parallel C code with OpenMP and MPI for the Brute Force algorithm in order to reduce its complexity.


*** *** The Brute Force algorithm *** *** 


This "naive" algorithm (check out nbody_brute_force.c) computes for each time interval the force on each particle in order to calculate its new position.

To compute the force on particle p1, one must compute the force that each particle exerts on particle p1 : 

	for(i=0; i < nparticles; i++) {
	    compute_force(p1, p[i]->x_pos, p[i]->y_pos, p[i]->mass);
	}


To compute the force applied on each particle, one must therefore make n*n calculations. Once these done, the computed force is applied for each particle : 


	for(i=0; i < nparticles; i++) {
	    move_particle(&particles[i], step);
	}