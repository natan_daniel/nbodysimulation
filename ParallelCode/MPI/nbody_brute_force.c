/*
** nbody_brute_force.c - nbody simulation using the brute-force algorithm (O(n*n))
**
**/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <sys/time.h>
#include <assert.h>
#include <unistd.h>
#include <mpi.h>

#ifdef DISPLAY
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#endif

#include "ui.h"
#include "nbody.h"
#include "nbody_tools.h"

FILE* f_out=NULL;

int nparticles=10;      /* number of particles */
float T_FINAL=1.0;     /* simulation end time */
particle_t* particles = NULL;

double sum_speed_sq = 0;
double max_acc = 0;
double max_speed = 0;

int world_size, world_rank; //number of processes and process' rank
int *sendcounts = NULL; //array describing how many elements to send to each process
int *displs = NULL; //array describing the displacements where each segment begins
particle_t *recvBuf = NULL; //Receiving buffer.

void init(int argc, char **argv) {
  // Initialize MPI environment
  int rc = MPI_Init(&argc, &argv);
  
  if(rc != MPI_SUCCESS) {
    fprintf(stderr, "Unable to set up MPI");
    MPI_Abort(MPI_COMM_WORLD, rc);
  }
}

#ifdef DISPLAY
Display *theDisplay;  /* These three variables are required to open the */
GC theGC;             /* particle plotting window.  They are externally */
Window theMain;       /* declared in ui.h but are also required here.   */
#endif

/* compute the force that a particle with position (x_pos, y_pos) and mass 'mass'
 * applies to particle p
 */
void compute_force(particle_t*p, double x_pos, double y_pos, double mass) {
  double x_sep, y_sep, dist_sq, grav_base;

  x_sep = x_pos - p->x_pos;
  y_sep = y_pos - p->y_pos;
  dist_sq = MAX((x_sep*x_sep) + (y_sep*y_sep), 0.01);

  /* Use The 2-Dimensional Gravity rule: F = d * (GMm/d^2) */
  grav_base = GRAV_CONSTANT*(p->mass)*(mass)/dist_sq;

  p->x_force += grav_base*x_sep;
  p->y_force += grav_base*y_sep;
}

/* compute the new position/velocity */
void move_particle(particle_t*p, double step, double* proc_sum_speed_sq, double* proc_max_acc, double* proc_max_speed) {

  p->x_pos += (p->x_vel)*step;
  p->y_pos += (p->y_vel)*step;
  double x_acc = p->x_force/p->mass;
  double y_acc = p->y_force/p->mass;

  p->x_vel += x_acc*step;
  p->y_vel += y_acc*step;

  /* compute statistics */
  double cur_acc = (x_acc*x_acc + y_acc*y_acc);
  cur_acc = sqrt(cur_acc);
  double speed_sq = (p->x_vel)*(p->x_vel) + (p->y_vel)*(p->y_vel);
  double cur_speed = sqrt(speed_sq);

  *proc_sum_speed_sq = *proc_sum_speed_sq + speed_sq;
  *proc_max_acc = MAX(*proc_max_acc, cur_acc);
  *proc_max_speed = MAX(*proc_max_speed, cur_speed);
}


/*
  Move particles one time step.

  Update positions, velocity, and acceleration.
  Return local computations.
*/
void all_move_particles(double step, double* proc_sum_speed_sq, double* proc_max_acc, double* proc_max_speed)
{
  /* First calculate force for particles. */
  int i;
  for(i=0; i<sendcounts[world_rank]; i++) {
    int j;
    recvBuf[i].x_force = 0;
    recvBuf[i].y_force = 0;
    for(j=0; j<nparticles; j++) {
      particle_t*p = &particles[j];
      /* compute the force of particle j on particle i */
      compute_force(&recvBuf[i], p->x_pos, p->y_pos, p->mass);
    }
  }

  /* then move all particles and return statistics */
  for(i=0; i<sendcounts[world_rank]; i++) {
    move_particle(&recvBuf[i], step, proc_sum_speed_sq, proc_max_acc, proc_max_speed);
  }
}

/* display all the particles */
void draw_all_particles() {
  int i;
  for(i=0; i<nparticles; i++) {
    int x = POS_TO_SCREEN(particles[i].x_pos);
    int y = POS_TO_SCREEN(particles[i].y_pos);
    draw_point (x,y);
  }
}

void run_simulation(double* proc_sum_speed_sq, double* proc_max_acc, double* proc_max_speed, int *sendcounts, int* displs, MPI_Datatype particleDt, int recvBufElements) {
  double t = 0.0, dt = 0.01;
  while (t < T_FINAL && nparticles>0) {
    /* Update time. */
    t += dt;
    
    /* Move particles with the current and compute rms velocity. */
    all_move_particles(dt, proc_sum_speed_sq, proc_max_acc, proc_max_speed);

    // Wait for all processes to finish computing.
    MPI_Barrier(MPI_COMM_WORLD);
    
    // Defining max_acc and max_speed.
    MPI_Allreduce(proc_max_acc, &max_acc, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(proc_max_speed, &max_speed, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    /* Update global shared particles array */
    MPI_Allgatherv(recvBuf, sendcounts[world_rank], particleDt, particles, sendcounts, displs, particleDt, MPI_COMM_WORLD);
    
    /* Adjust dt based on maximum speed and acceleration--this
       simple rule tries to insure that no velocity will change
       by more than 10% */

    dt = 0.1*max_speed/max_acc;

    if(world_rank == 0) {
      #if DISPLAY
      clear_display();
      draw_all_particles();
      flush_display();
      #endif
    }

    //Waiting for process 0 to finish drawing
    MPI_Barrier(MPI_COMM_WORLD);
  }
}

/*
  Simulate the movement of nparticles particles.
*/
int main(int argc, char**argv)
{  

  if(argc >= 2) {
    nparticles = atoi(argv[1]);
  }
  if(argc == 3) {
    T_FINAL = atof(argv[2]);
  }

  int rem = 0; //elements remaining after division among processes
  int sum = 0; //Sum of counts. Used to calculate displacements.
  int recvBufElements = 0; //total amount of elements in receiving buffer 

  //Allocate global shared array for the particles data set.
  particles = malloc(sizeof(particle_t)*nparticles);

  //Place particles in their initial positions.
  all_init_particles(nparticles, particles);

  //Initialize the MPI environment
  init(argc, argv);

  //Get the number of processes
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  
  //Get the rank of the process
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  //Define particle MPI_Datatype
  MPI_Datatype particleDt;
  MPI_Type_contiguous(7, MPI_DOUBLE, &particleDt);
  MPI_Type_commit(&particleDt);

  if(world_rank == 0) {
    /* Initialize thread data structures */
    #ifdef DISPLAY
    /* Open an X window to display the particles */
    simple_init (100,100,DISPLAY_SIZE, DISPLAY_SIZE);
    #endif
  }

  //Process specific sum_speed, max_acc and max_speed variables for later computations
  double proc_sum_speed_sq = 0;
  double proc_max_acc = 0;
  double proc_max_speed = 0;

  /*
  Preparing MPI_Scatter call
  */
  rem = nparticles%world_size;
  sendcounts = malloc(sizeof(int)*world_size);
  displs = malloc(sizeof(int)*world_size);
  recvBufElements = (nparticles/world_size) + rem;
  recvBuf = malloc(sizeof(particle_t)*recvBufElements);

  //Calculate send counts and displacements
  int i;
  for(i = 0; i < world_size; i++) {
      sendcounts[i] = nparticles/world_size;
      if (rem > 0) {
         sendcounts[i]++;
          rem--;
      }
      displs[i] = sum;
      sum += sendcounts[i];
  }
  
  //Divide the data among processes as described by sendcounts and displs
  MPI_Scatterv(particles, sendcounts, displs, particleDt, recvBuf, recvBufElements, particleDt, 0, MPI_COMM_WORLD);

  struct timeval t1, t2;
  gettimeofday(&t1, NULL);

  //All processes start computing on their sub arrays and update the global particles array
  run_simulation(&proc_sum_speed_sq,&proc_max_acc, &proc_max_speed, sendcounts, displs, particleDt, recvBufElements);

  //MPI_Barrier(MPI_COMM_WORLD);

  gettimeofday(&t2, NULL);

  double duration = (t2.tv_sec -t1.tv_sec)+((t2.tv_usec-t1.tv_usec)/1e6);
    
  if(world_rank == 0) {
    
    printf("-----------------------------\n");
    printf("nparticles: %d\n", nparticles);
    printf("T_FINAL: %f\n", T_FINAL);
    printf("-----------------------------\n");
    printf("Simulation took %lf s to complete\n", duration);
    
    #ifdef DUMP_RESULT
    FILE* f_out = fopen("particles.log", "w");
    assert(f_out);
    print_particles(f_out, root);
    fclose(f_out);
    #endif

    #ifdef DISPLAY
    clear_display();
    draw_all_particles();
    flush_display();

    printf("Hit return to close the window.");

    getchar();
    /* Close the X window used to display the particles */
    XCloseDisplay(theDisplay);
    #endif
  }

  //Finalize the MPI environment.
  MPI_Finalize();

 free(particles);
 free(sendcounts);
 free(displs);
 free(recvBuf);
  
  return 0;
}
